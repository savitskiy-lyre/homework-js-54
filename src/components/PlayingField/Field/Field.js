import React from 'react';
import './Field.css';


const Field = ({isFLip, onOpenField, hasItem}) => {
   const fieldClasses = ['field'];
   if (!isFLip) {
      fieldClasses.push('grey-bg');
   }
   let item = null;
   let closeEl = null;
   if (hasItem && isFLip) {
      item = 'O';
      closeEl = <div style={{
         position: 'absolute',
         display: 'flex',
         alignItems: 'center',
         justifyContent: 'center',
         color: 'moccasin',
         left: '0',
         top: '0',
         width: '100%',
         height: '100%',
         background: 'rgba(0,0,0,0.35)',
         fontSize: '40px',
      }}>You Win !!!</div>
   }
   return (
     <div className={fieldClasses.join(' ')} onClick={onOpenField}>
        <span>{item}</span>
        {closeEl}
     </div>
   );
};

export default Field;