import {nanoid} from "nanoid";
import React from 'react';
import Field from "./Field/Field";
import './PlayingField.css';

const PlayingField = ({fieldsState, onOpenField}) => {
   const fieldsEl = [];
   for (const field of fieldsState) {
      fieldsEl.push(<Field id={field.id} isFLip={field.isFlip} onOpenField={()=>onOpenField(field.id)} hasItem={field.hasItem} key={nanoid()}/>)
   }
   return (
     <div className='PlayingField-game'>
        <div className='PlayingField-wrapper'>
           {fieldsEl}
        </div>
     </div>
   );
};

export default PlayingField;