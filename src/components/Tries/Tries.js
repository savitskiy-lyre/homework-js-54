import React from 'react';

const Tries = ({count}) => {
   return (
     <p>Tries: {count}</p>
   );
};

export default Tries;